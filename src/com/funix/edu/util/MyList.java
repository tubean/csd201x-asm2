package com.funix.edu.util;

import com.funix.edu.entity.Book;

public class MyList {
    
    Node<Book> head, tail;
    
    //ctor
    public MyList() {
        head = tail = null;
    }
    //check if the list is empty or not
    public boolean isEmpty() {
        return head == null;
    }
    //add a new Book to the end of list
    public void addLast(Book b) {
        Node p = new Node(b);
        if(isEmpty()) head = tail = p;
        else {
            tail.next = p;
            tail = p;
        }
    }
    //add a new Book to the begining of list
    public void addFirst(Book b) {
        Node<Book> p = new Node(b);
        if(isEmpty()) head = tail = p;
        else {
            p.next = head;
            head = p;
        }
    }
    //output information of all books in the list
    public void traverse() {
        if(isEmpty()) {
            System.out.println("No book to show...");
            return;
        }
        System.out.println(String.format("%-10s%-20s%-10s%-10s%-10s%-10s", "Code","Title",
                "Quantity","Lender","Price","Value"));
        Node<Book> p = head;
        while(p != null) {
            System.out.println(p.info);
            p = p.next;
        }
    }
    //return number of nodes/elements in the list
    public int size() {
        int cnt = 0;
        if(isEmpty()) return cnt;
        Node p = head;
        while(p != null) {
            cnt ++;
            p = p.next;
        }
        return cnt;
    }
    //return a Node at position k, starting position is 0
    public Node<Book> getNode(int k) {
        if(isEmpty()) return null;
        int i = 0;
        Node<Book> p = head;
        while(p != null && i < k) {
            p = p.next;
            i++;
        }
        return p;
    }
    //add a new book after a position k
    public void addAfter(Book b, int k) {
        int n = size();
        if(k < 0 || k >= n) {
            System.out.println("Inserting position is invalid");
            return;
        }
        Node<Book> p = new Node(b);
        Node<Book> q = getNode(k);
        //insert p after q
        p.next = q.next;
        q.next = p;
        if(q == tail) tail = p;
    }
    //delete a book at position k
    public void deleteAt(int k, boolean printMessage) {
        int n = size();
        if(k < 0 || k >= n) {
            if (printMessage)
            System.out.println("deleting position is invalid");
            return;
        }
        //get a node before k
        Node<Book> p = getNode(k-1);
        //remove head
        if(k == 0) head = head.next;
        //remove tail
        else if(k == n - 1) {
            p.next = null;
            tail = p;
        }else {
            p.next = p.next.next;
        }
        if(printMessage)
        System.out.println("A book at position " + k + " has been deleted");
    }
    //search a Node by a given book code
    public Node<Book> search(String bCode) {
        if(isEmpty()) return null;
        Node<Book> p = head;
        while(p != null) {
            if(p.info.getbCode().equalsIgnoreCase(bCode)) return p;
            p = p.next;
        }
        return null;
    }
    
}
