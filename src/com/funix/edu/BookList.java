package com.funix.edu;

import com.funix.edu.entity.Book;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;
import com.funix.edu.util.MyList;
import com.funix.edu.util.Node;

public class BookList implements Serializable {

    //a list of book
    private MyList books;

    public static final String FILE_NAME = "asm2.txt";

    public MyList getBooks() {
        return books;
    }


    public BookList() {
        books = new MyList();
    }

    //1.0 accept information of a Book
    private Book getBook() {
        String bCode, title;
        int quantity, lended;
        double price;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter information of a Book");
        //book code must be unique
        while (true) {
            System.out.print("Book Code: ");
            bCode = in.nextLine();
            if (books.search(bCode) != null) {
                System.out.println("Book code must be unique");
            } else {
                break;
            }
        }
        //book title cannot be empty
        while (true) {
            System.out.print("Book Title: ");
            title = in.nextLine();
            if (title.trim().equals("")) {
                System.out.println("Book title cannot be empty");
            } else {
                break;
            }
        }
        //lended must be less than or equals to quantity
        while (true) {
            System.out.print("Book Quanity: ");
            quantity = Integer.valueOf(in.nextLine());
            System.out.print("Book Lended: ");
            lended = Integer.valueOf(in.nextLine());
            if (lended > quantity) {
                System.out.println("Lended must be greater than or equals to quantity");
            } else {
                break;
            }
        }
        System.out.print("Book Price: ");
        price = Double.valueOf(in.nextLine());
        return new Book(bCode, title, quantity, lended, price);
    }

    //1.1.0
    public void addLast(Book b) {
        books.addLast(b);
    }
    //1.1 accept and add a new Book to the end of book list
    public void addLast() {
        books.addLast(getBook());
        System.out.println("A new book has been added to the last position of the list");
    }

    //1.2 output information of book list
    public void list() {
        books.traverse();
    }

    //1.3 search book by book code
    public void search() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter book code: ");
        String bCode = "";
        Node<Book> p = books.search(bCode = in.nextLine());
        if (p == null) {
            System.out.println("Data not found");
        } else {
            System.out.println("Information of book code " + bCode);
            System.out.println(p.info);
        }
    }

    //1.4 accept and add a new Book to the begining of book list
    public void addFirst() {
        books.addFirst(getBook());
        System.out.println("A new book has been added to the first position of the list");
    }

    public void addFirst(Book b) {
        books.addFirst(b);
    }


    //1.5 Add a new Book after a position k
    public void addAfter() {
        Scanner in = new Scanner(System.in);
        Book b = getBook();
        System.out.print("Enter adding position: ");
        int k = Integer.valueOf(in.nextLine());
        books.addAfter(b, k);
        System.out.println("A new book has been added after position " + k);
    }

    //1.6 Delete a Book at position k
    public void deleteAt() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter deleting position: ");
        int k = Integer.valueOf(in.nextLine());
        books.deleteAt(k,true);
    }

    //1.7 Export Book list to file
    public void exportToFile() {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(BookList.FILE_NAME));
            writer.write(String.format("%-10s%-20s%-10s%-10s%-10s%-10s", "Code","Title",
                    "Quantity","Lender","Price","Value") + "\n");
            Node<Book> p = books.getNode(0);
            while (p != null) {
                writer.write(p.info.toString());
                writer.write("\n");
                p = p.next;
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //1.8 Order list by code
    public void orderByCode(BookList bookList) {
        if(bookList.books.size() < 2) return;
        MyList resultList = bookList.books;
        for(int i = 0; i < bookList.books.size(); i++) {
            int index = i;
            for(int j = i +1; j < resultList.size(); j++) {
                String codeJ = resultList.getNode(j).info.getbCode();
                String codeIndex = resultList.getNode(index).info.getbCode();
                if (codeJ.compareToIgnoreCase(codeIndex) > 0) {
                    index = j;
                }
            }
            resultList.addFirst(bookList.books.getNode(index).info);
            resultList.deleteAt(index+1,false);
        }
        list();
    }

    //1.9 Order list by Price
    public void orderByPrice(BookList bookList) {
        if(bookList.books.size() < 2) return;
        MyList resultList = bookList.books;
        for(int i = 0; i < bookList.books.size(); i++) {
            int index = i;
            for(int j = i +1; j < resultList.size(); j++) {
                double codeJ = resultList.getNode(j).info.getPrice();
                double codeIndex = resultList.getNode(index).info.getPrice();
                if (codeJ > codeIndex) {
                    index = j;
                }
            }
            resultList.addFirst(bookList.books.getNode(index).info);
            resultList.deleteAt(index+1,false);
        }
        list();
    }
}
