package com.funix.edu;

import com.funix.edu.entity.Book;

import java.util.Scanner;

public class Main {
    
    static int getMenuItem() {
        System.out.print("Book List\n1.Input Book and add to the end\n2.Display books"
                + "\n3.Search by code\n4.Input Book and add to beginning\n5.Add Book after position k"
                + "\n6.Delete Book at position k\n7.Export list of books to file asm2.txt"
                + "\n8.Order list by code (asc)\n9.Order list by price (asc)\n0.Exit\nYour choice: ");
        return Integer.valueOf(new Scanner(System.in).nextLine());
    }
    //add some books to the list
    static void addBooks(BookList books) {
        books.addLast(new Book("B03", "Morning", 12, 0, 25.1));
        books.addLast(new Book("B01", "The noon", 10, 0, 5.2));
        books.addLast(new Book("B02", "River", 5, 0, 4.3));
        books.addLast(new Book("B05", "Physics", 7, 0, 15.4));
        books.addLast(new Book("B07", "Biology", 11, 0, 12.2));
        books.addLast(new Book("B04", "Southern", 9, 0, 5.2));
    }
   
    public static void main(String[] args) {
        int c = -1;
        BookList books = new BookList();
        //add some books, readers and lendings to list
        addBooks(books);
        while(c != 0) {
            c = getMenuItem();
            switch(c) {
                case 1: books.addLast();break;
                case 2: books.list();break;
                case 3: books.search();break;
                case 4: books.addFirst();break;
                case 5: books.addAfter();break;
                case 6: books.deleteAt();break;
                case 7: books.exportToFile();break;
                case 8: books.orderByCode(books);break;
                case 9: books.orderByPrice(books);break;
                case 0: return;
                default: System.out.println("Invalid choice...");
            }
        }
    }
}
